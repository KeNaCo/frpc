from collections import OrderedDict
from datetime import datetime
from unittest import TestCase

import frpc


class TestFastRPCEncodeV1(TestCase):
    def test_unknown_object(self):
        class X:
            pass

        x = X()
        with self.assertRaises(frpc.FastRPCEncoderError):
            frpc.dumps(x)

    def test_int_zero(self):
        self.assertEqual(frpc.dumps(0), b'\x08\x00')

    def test_int_one(self):
        self.assertEqual(frpc.dumps(1), b'\x08\x01')

    def test_int_two_bytes(self):
        self.assertEqual(frpc.dumps(257), b'\x09\x01\x01')

    def test_int_minus_is_not_considered_in_v1(self):
        with self.assertRaises(OverflowError):
            frpc.dumps(-1)

    def test_bool_true(self):
        self.assertEqual(frpc.dumps(True), b'\x11')

    def test_bool_false(self):
        self.assertEqual(frpc.dumps(False), b'\x10')

    def test_float_simple(self):
        self.assertEqual(frpc.dumps(0.5), b'\x18\x00\x00\x00\x00\x00\x00\xe0?')

    def test_string_ascii(self):
        self.assertEqual(frpc.dumps(u'abcd'), b'\x20\x04abcd')

    def test_string_special_characters(self):
        self.assertEqual(frpc.dumps(u'ábčd'), b'\x20\x06\xc3\xa1b\xc4\x8dd')

    def test_datetime_simple(self):
        d = datetime(year=2017, month=4, day=2, hour=12, minute=8, second=1)
        self.assertEqual(frpc.dumps(d), b'(\x00\x81\xcd\xe0X\x08\x10&(4')

    def test_bytes_empty(self):
        self.assertEqual(frpc.dumps(b''), b'0\x00')

    def test_bytes(self):
        self.assertEqual(frpc.dumps(b'\x01\x02\x03'), b'0\x03\x01\x02\x03')

    def test_struct_empty_dict(self):
        self.assertEqual(frpc.dumps({}), b'\x50\x00')

    def test_struct_all_item_types(self):
        d = OrderedDict({
            'int': 1,
            'bool': True,
            'float': 1.2,
            'str': 'čeče'
        })
        self.assertEqual(frpc.dumps(d), b'P\x04\x03int\x08\x01\x04bool\x11\x05float\x18333333\xf3?\x03str \x06\xc4\x8de\xc4\x8de')

    def test_struct_empty_name(self):
        with self.assertRaises(TypeError):
            frpc.dumps({'': 1})

    def test_struct_none_name(self):
        with self.assertRaises(TypeError):
            frpc.dumps({None: 1})

    def test_array_list_empty(self):
        self.assertEqual(frpc.dumps([]), b'X\x00')

    def test_array_list_all_item_types(self):
        array = [1, 1.0, False, "šašo"]
        self.assertEqual(frpc.dumps(array), b'X\x04\x08\x01\x18\x00\x00\x00\x00\x00\x00\xf0?\x10 \x06\xc5\xa1a\xc5\xa1o')

    def test_array_tuple_empty(self):
        self.assertEqual(frpc.dumps(tuple()), b'X\x00')

    def test_array_tuple_all_item_types(self):
        array = (1, 1.0, False, "šašo")
        self.assertEqual(frpc.dumps(array), b'X\x04\x08\x01\x18\x00\x00\x00\x00\x00\x00\xf0?\x10 \x06\xc5\xa1a\xc5\xa1o')

    def test_array_iterator_empty(self):
        self.assertEqual(frpc.dumps(x for x in range(0)), b'X\x00')

    def test_array_iterator_all_types(self):
        self.assertEqual(frpc.dumps(x for x in range(5)), b'X\x05\x08\x00\x08\x01\x08\x02\x08\x03\x08\x04')

    def test_method_call_empty_method_name(self):
        with self.assertRaises(ValueError):
            call = frpc.MethodCall('')
            frpc.dumps(call)

    def test_method_call_too_long_method_name(self):
        with self.assertRaises(ValueError):
            call = frpc.MethodCall('a' * 256)
            frpc.dumps(call)

    def test_method_call_empty_params(self):
        call = frpc.MethodCall('some_name')
        self.assertEqual(frpc.dumps(call), b'\xca\x11\x01\x00h\tsome_name')

    def test_method_call(self):
        call = frpc.MethodCall('some_name', 257)
        self.assertEqual(frpc.dumps(call), b'\xca\x11\x01\x00h\tsome_name\x09\x01\x01')

    def test_method_response_empty(self):
        response = frpc.MethodResponse()
        self.assertEqual(frpc.dumps(response), b'\xca\x11\x01\x00\x70')

    def test_method_response_explicit_empty_value(self):
        response = frpc.MethodResponse(frpc.MethodResponse.Empty())
        self.assertEqual(frpc.dumps(response), b'\xca\x11\x01\x00\x70')

    def test_method_response(self):
        response = frpc.MethodResponse(257)
        self.assertEqual(frpc.dumps(response), b'\xca\x11\x01\x00\x70\x09\x01\x01')

    def test_method_response_method_call_value(self):
        with self.assertRaises(ValueError):
            frpc.MethodResponse(frpc.MethodCall('some_method'))

    def test_method_response_method_response_value(self):
        with self.assertRaises(ValueError):
            frpc.MethodResponse(frpc.MethodResponse())

    def test_fault_response_empty_message(self):
        fault = frpc.FaultResponse(0, '')
        self.assertEqual(frpc.dumps(fault), b'\xca\x11\x01\x00\x78\x08\x00\x20\x00')

    def test_fault_response(self):
        fault = frpc.FaultResponse(1, 'some_text')
        self.assertEqual(frpc.dumps(fault), b'\xca\x11\x01\x00\x78\x08\x01\x20\tsome_text')


class TestFastRPCDecodeV1(TestCase):
    def test_int_zero(self):
        o = frpc.loads(b'\x08\x00')
        self.assertEqual(o, 0)
        self.assertTrue(isinstance(o, int))

    def test_int_one(self):
        o = frpc.loads(b'\x08\x01')
        self.assertEqual(o, 1)
        self.assertTrue(isinstance(o, int))

    def test_int_two_bytes(self):
        o = frpc.loads(b'\x09\x01\x01')
        self.assertEqual(o, 257)
        self.assertTrue(isinstance(o, int))

    def test_bool_true(self):
        o = frpc.loads(b'\x11')
        self.assertEqual(o, True)
        self.assertTrue(isinstance(o, bool))

    def test_bool_false(self):
        o = frpc.loads(b'\x10')
        self.assertEqual(o, False)
        self.assertTrue(isinstance(o, bool))

    def test_float_simple(self):
        o = frpc.loads(b'\x18\x00\x00\x00\x00\x00\x00\xe0?')
        self.assertEqual(o, 0.5)
        self.assertTrue(isinstance(o, float))

    def test_string_ascii(self):
        o = frpc.loads(b'\x20\x04abcd')
        self.assertEqual(o, u'abcd')
        self.assertTrue(isinstance(o, str))

    def test_string_special_characters(self):
        o = frpc.loads(b'\x20\x06\xc3\xa1b\xc4\x8dd')
        self.assertEqual(o, u'ábčd')
        self.assertTrue(isinstance(o, str))

    def test_datetime_simple(self):
        o = frpc.loads(b'(\x00\x81\xcd\xe0X\x08\x10&(4')
        d = datetime(year=2017, month=4, day=2, hour=12, minute=8, second=1)
        self.assertEqual(o, d)
        self.assertTrue(isinstance(o, datetime))

    def test_bytes_empty(self):
        o = frpc.loads(b'0\x00')
        self.assertEqual(o, b'')
        self.assertTrue(isinstance(o, bytes))

    def test_bytes(self):
        o = frpc.loads(b'0\x03\x01\x02\x03')
        self.assertEqual(o, b'\x01\x02\x03')
        self.assertTrue(isinstance(o, bytes))

    def test_struct_empty_dict(self):
        o = frpc.loads(b'\x50\x00')
        self.assertEqual(o, {})
        self.assertTrue(isinstance(o, dict))

    def test_struct_all_item_types(self):
        o = frpc.loads(b'P\x04\x03int\x08\x01\x04bool\x11\x05float\x18333333\xf3?\x03str \x06\xc4\x8de\xc4\x8de')
        d = OrderedDict({
            'int': 1,
            'bool': True,
            'float': 1.2,
            'str': 'čeče'
        })
        self.assertEqual(o, d)
        self.assertTrue(isinstance(o, dict))

    def test_array_list_empty(self):
        o = frpc.loads(b'X\x00')
        self.assertEqual(o, [])
        self.assertTrue(isinstance(o, list))

    def test_array_list_all_item_types(self):
        o = frpc.loads(b'X\x04\x08\x01\x18\x00\x00\x00\x00\x00\x00\xf0?\x10 \x06\xc5\xa1a\xc5\xa1o')
        self.assertEqual(o, [1, 1.0, False, "šašo"])
        self.assertTrue(isinstance(o, list))

    def test_method_call_empty_params(self):
        o = frpc.loads(b'\xca\x11\x01\x00h\tsome_name')
        call = frpc.MethodCall('some_name')
        self.assertEqual(o, call)
        self.assertTrue(isinstance(o, frpc.MethodCall))

    def test_method_call(self):
        o = frpc.loads(b'\xca\x11\x01\x00h\tsome_name\x09\x01\x01')
        call = frpc.MethodCall('some_name', 257)
        self.assertEqual(o, call)
        self.assertTrue(isinstance(o, frpc.MethodCall))

    def test_method_response_empty(self):
        o = frpc.loads(b'\xca\x11\x01\x00\x70')
        response = frpc.MethodResponse()
        self.assertEqual(o, response)
        self.assertTrue(isinstance(o, frpc.MethodResponse))

    def test_method_response(self):
        o = frpc.loads(b'\xca\x11\x01\x00\x70\x09\x01\x01')
        response = frpc.MethodResponse(257)
        self.assertEqual(o, response)
        self.assertTrue(isinstance(o, frpc.MethodResponse))

    def test_fault_response_empty_message(self):
        o = frpc.loads(b'\xca\x11\x01\x00\x78\x08\x00\x20\x00')
        fault = frpc.FaultResponse(0, '')
        self.assertEqual(o, fault)
        self.assertTrue(isinstance(o, frpc.FaultResponse))

    def test_fault_response(self):
        o = frpc.loads(b'\xca\x11\x01\x00\x78\x08\x01\x20\tsome_text')
        fault = frpc.FaultResponse(1, 'some_text')
        self.assertEqual(o, fault)
        self.assertTrue(isinstance(o, frpc.FaultResponse))
