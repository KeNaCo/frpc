# frpc

Pure python implementation of [FastRPC protocol](https://github.com/seznam/fastrpc/wiki/FastRPC-binary-protocol-specification)

**Library is under heavy development. Anything can change at any time.**

## Installation

```
python3 setup.py install
```

## Getting started

Simple data serialisation:

```python
import frpc
data = "some data"
binary_str = frpc.dumps(data)
data2 = frpc.loads(binary_str)
```
