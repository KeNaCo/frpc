from os import path
from setuptools import setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='frpc',
    version='0.1.0',
    description='Pure python implementation of binary FastRPC format de/serialisation',
    long_description=long_description,
    url='https://github.com/KeNaCo/frpc',
    author='KeNaCo',
    author_email='kenaco666@gmail.com',
    license='MIT',
    classifiers=[
        # 'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        # 'Development Status :: 4 - Beta',
        # 'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='RPC FastRPC',
    packages=['frpc'],
    python_requires='~=3.6',
    setup_requires=['pytest-runner', 'wheel'],
    tests_require=['pytest']
)
