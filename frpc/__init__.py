from typing import Any
from .common import MethodCall, MethodResponse, FaultResponse
from .serializers import get_serializer, FastRPCEncoderError
from .deserializers import get_deserializer, View


def dumps(obj: Any, *, version: int=1) -> bytes:
    """ Encode obj to FastRPC binary format """
    # we store used protocol version
    serializer = get_serializer(version)

    return serializer.encode(obj)


def loads(data: bytes, *, version: int=1) -> Any:
    """ Decode fastrpc encoded data to python obj """
    view = View(data)
    if data.startswith(b'\xca\x11'):
        view.next(size=2)
        # TODO here we can get serializer version
        view.next(size=2)
    deserializer = get_deserializer(version)

    return deserializer.decode(view)
